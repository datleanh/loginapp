//
//  File.swift
//  LoginApp
//
//  Created by Dat Le Anh on 2/15/19.
//  Copyright © 2019 Dat Le Anh. All rights reserved.
//

import Foundation
class UserDefaultTools {
    static var users: [User] {
        let defaultList: [User] = []
        if let userList = UserDefaults.standard.value(forKey: "user_list") as? Data {
            let decoder = JSONDecoder()
            if let userDecoded = try? decoder.decode(Array.self, from: userList) as [User] {
                return userDecoded
            } else {
                return defaultList
            }
            } else {
                return defaultList
         }
    }
    
    
    static func saveUser(users: [User]) -> Bool {
        let encode = JSONEncoder()
        if let encoded = try? encode.encode(users) {
            UserDefaults.standard.set(encoded, forKey: "user_list")
        }
        return true
    }
}
