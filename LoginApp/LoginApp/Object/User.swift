//
//  User.swift
//  LoginApp
//
//  Created by Dat Le Anh on 2/15/19.
//  Copyright © 2019 Dat Le Anh. All rights reserved.
//

import Foundation

struct User: Codable {
    let username: String?
    let password: String?
}
