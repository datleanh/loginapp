//
//  ViewController.swift
//  LoginApp
//
//  Created by Dat Le Anh on 2/15/19.
//  Copyright © 2019 Dat Le Anh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func doLogin(_ sender: Any) {
        guard let username = usernameTextField.text, let password = passwordTextField.text else { return }
        let userList = UserDefaultTools.users
        let users = userList.filter({$0.password == password && username == username})
        if users.count != 0 {
            print("Login Success")
            guard let homeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else { return }
            self.present(homeViewController, animated: true, completion: nil)
        }
    }
    
}

