//
//  SignUpViewController.swift
//  LoginApp
//
//  Created by Dat Le Anh on 2/15/19.
//  Copyright © 2019 Dat Le Anh. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func signUp(username: String, password: String) -> Bool {
        let newUser = User(username: username, password: password)
        var userList = UserDefaultTools.users
        
        
         let user = userList.filter({ $0.username == newUser.username })
            if user.count == 0 {
                return false
            } else {
                userList.append(newUser)
                let _ = UserDefaultTools.saveUser(users: userList)
            }
        return true
    }
    
    @IBAction func doSignUp(_ sender: Any) {
        guard let username = usernameTextField.text, let password = passwordTextField.text, let confirmPassword = confirmPasswordTextField.text else { return }
        
        if confirmPassword != password {
            print("password not the same!")
            return
        } else {
            if signUp(username: username, password: password) {
                print("sign up Success")
                self.dismiss(animated: true, completion: nil)
            } else {
                print("sign up Failed")
            }
        }
    }
}
